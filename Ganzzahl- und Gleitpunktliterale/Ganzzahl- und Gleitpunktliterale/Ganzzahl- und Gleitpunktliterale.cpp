#include<iostream>
#include<string>
using namespace std;
int main(void)
{
	int a10{ 10 };
	int a8{ 012 };
	int a16{ 0xA };
	int a2{ 0b1010 };
	long b10{ 1000000000l };
	long b2{ 0b0111011100110101100101000000000L };
	long b8{ 07346545000L };
	long b16{ 0x3B9ACA00L };
	float pi_g{ 3.1415927f };
	float pi_e{ 0.31415927E01f };
	double e_g{ 2.718281828459045 };
	double e_e{ 0.2718281828459045E01 };
	cout << a2 << endl;
	cout << a8 << endl;
	cout << a10 << endl;
	cout << a16 << endl;
	cout << b2 << endl;
	cout << b8 << endl;
	cout << b10 << endl;
	cout << b16 << endl;
	cout << pi_g << endl;
	cout << pi_e << endl;
	cout << e_g << endl;
	cout << e_e << endl;
	system("pause");
	return 0;
}